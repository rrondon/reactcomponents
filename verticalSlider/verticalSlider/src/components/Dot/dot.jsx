import React from 'react';
import './dot.css';

const Dot = ({ id, active, dotClick }) => {
  const dotClass = active ? 'dot active' : 'dot'

  return (
      <div className={dotClass}
        onClick={() => dotClick(id)}
        />
   )
  }

export default Dot;
