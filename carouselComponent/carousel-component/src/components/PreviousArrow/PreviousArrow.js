import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

const PreviousArrow = (props) => {
  const { onClick } = props;

  return (
   <div
     className="leftArrow arrow"
     onClick={onClick}
    >
      <FontAwesomeIcon icon={faAngleLeft} />
    </div>
  )
}

export default PreviousArrow;
