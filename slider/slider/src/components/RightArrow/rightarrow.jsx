import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import './rightarrow.css';

const RightArrow = (props) => {
  return(
    <div className="rigtArrow arrow" onClick={props.goToNextSlide}>
      <FontAwesomeIcon icon={faAngleRight} />
    </div>
  );
}

export default RightArrow;
