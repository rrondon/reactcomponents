import React, { Component } from 'react';
import ArticleNav from '../ArticleNav/ArticleNav';
import './rightrail.css';

const RightRail = ({ data, index, description, loadMore, visible, likes, articleClick }) => {

  const theArticles = data.slice(0, visible).map((article, i) => {
    let active = ( i === index ) ? true : false

    return (
      <ArticleNav
            title="ArticleNav"
            key={i}
            id={i}
            active={active}
            description={article.description}
            loadMore={loadMore}
            likes={article.likes}
            articleClick={articleClick}
      />
    )
  })

    return (
    <div className="articlesDiv sticky">
      { theArticles }
      { visible < data.length &&
        <button onClick={loadMore} type="button" className="loadMoreBtn">Load More</button>
      }
    </div>
  )
}

export default RightRail;
