import React, { Component } from 'react';
import Img from '../Img/img';
import './imglist.css';

class ImgList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            img: '',
            description: '',
            id: '',
            likes: 0,
            visible: 8,
            count: 0,
            isLoading: false,
        }
        this.loadMore = this.loadMore.bind(this);
    }

    loadMore() {
        this.setState((prev) => {
            return {
                visible: prev.visible + 2
            }
        });
    }

    showImgs() {
      return this.props.data.slice(0, this.state.visible).map((img) => (
         <Img
           key={img.id}
           img={img.urls.small}
           description={img.description}
           likes={img.likes}
         />
      ));
    }

    render() {

        return (
           <div className="searchCardGrid">
             <div className="cardLayout">
                {this.showImgs()}
            </div>

            {this.state.visible < this.props.data.length &&
               <button onClick={this.loadMore} type="button" className="loadMoreBtn">Load More Posts</button>
            }
          </div>
        );
    }
};

export default ImgList;
