import React from 'react';

const Img = (props) => {
    const { img, id, description, likes} = props

    return (
        <div className="cardDiv">
            <div className="Card">
                <div className="cardContent">
                    <img className="cardImg" key={props.id} src={props.img} alt=''></img>
                    <div className="cardDescription">
                        <p className="imgInfo">{props.description}</p>
                        <p className="imgLikes">Total Likes: {props.likes}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Img;
