import React, { Component } from 'react';
import AccordionItem from '../AccordionItem/accordionitem';
import './accordion.css';

class Accordion extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: '',
      description: '',
      isLoading: false,
      bio: '',
      data: [],
      open: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      open: !this.state.open
    })
  }

    componentDidMount() {
      fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
        method: 'GET',
    })
    .then((results) => {
      results.json().then((data) => {
        this.setState({
          data: data,
          id: data[0].id,
          description: data[0].description,
          bio: data[0].bio,
        })
      })
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  }

  componentWillUnmount() {
    this.setState({
      isLoading: false
    })
  }

  showAccordions() {
    return this.state.data.map((accordion) => (
      <AccordionItem
        key={accordion.id}
        description={accordion.description}
        bio={accordion.bio}
      />
    ))
  }

  render() {

    const { open } = this.state;

    return(
      <div className="showAccordions">
        {this.showAccordions()}
      </div>
    );
  }
}

export default Accordion;
