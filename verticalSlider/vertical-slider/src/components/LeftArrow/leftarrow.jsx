import React from 'react';
import './leftarrow.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

const LeftArrow = (props) => {
  return (
    <div className="leftArrow arrow" onClick={props.goToPrevSlide}>
      <FontAwesomeIcon icon={faAngleLeft} />
    </div>
  );
}

export default LeftArrow
