## React Accordion ##

## Setup ##

This accordion was initially setup with Gatsby (https://www.gatsbyjs.org)

## Installation Steps ##

1. `git clone https://rrondon@bitbucket.org/rrondon/reactcomponents.git`
2. `cd reactcomponents/Accordion/accordion`
3. `npm install`
4. `npm run develop`
5. Visit `localhost:8000` in your browser of choice 
