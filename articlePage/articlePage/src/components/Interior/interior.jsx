import React, { Component } from 'react';
import Slide from '../Slide/slide';
import Scroll from '../Scroll/scroll';
import RightRail from '../RightRail/rightrail';
import './interior.css';

class Interior extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count: 0,
      img: '',
      data: [],
      id: '',
      description: '',
      isLoading: false,
      likes: 0,
      visible: 5,
      index: 0,
      slideIndex: 0
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {
        visible: prev.visible + 5
      }
    })
  }

  componentDidMount() {
    fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
      method: 'GET',
    })
    .then((results) => {
      results.json().then((data) => {
        this.setState({
          data: data,
          id: data[0].id,
          img: data[0].urls.full,
          description: data[0].description,
          isLoading: true
        })
      })
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  }

  componentWillUnmount = () => {
    this.setState({
      isLoading: false
    })
  }

    articleClick = (i) => {
        if (i === this.state.index) {
            return
        } else {
            this.setState({
                img: this.state.data[i].urls.full,
                id: this.state.data[i].id,
                index: i,
                slideIndex: i
            })
        };
    }

  showArticles() {
      return this.state.data.slice(0, this.state.visible).map((article, i) => (
          <Slide
              key={article.id}
              img={article.urls.small}
              description={article.description}
              likes={article.likes}
              slideIndex={i}
          />
      ));
    }
        
    render() {
        console.log('this is the index');
        console.log(this.state.index);

        console.log('this is the slide index');
        console.log(this.state.slideIndex);
    return(
      <div className="interiorDiv">
        <Scroll />
        <div className="articlePageLayout">
          <div className="leftPanel">
                    {this.showArticles()}
          {this.state.visible < this.state.data.length &&
            <button onClick={this.loadMore} type="button" className="loadMoreBtn">Load More Posts</button>
          }
        </div>
        <div className="rightPanel">
          <RightRail
             data={this.state.data}
             index={this.state.index}
             description={this.state.description}
             loadMore={this.loadMore}
             visible={this.state.visible}
             likes={this.state.likes}
             articleClick={this.articleClick}
          />
        </div>
        </div>
      </div>
    );
  }
}

export default Interior;
