import React, { Component } from 'react';
import axios from 'axios';
import ImgList from '../ImgList/imglist';

class SearchForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      data: [],
      visible: 8,
      value: '',
      q: ''
    }
    this.loadMore = this.loadMore.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {
        visible: prev.visible + 2
      }
    });
  }

  componentDidMount() {
    this.handleSubmit();
  }

  handleSubmit = (q = this.state.q, value = this.state.value) => {
    axios
      .get(
        `https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${q}&query=${value}&client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1`
      )
      .then(data => {
        this.setState({
          data: data.data.results,
          isLoading: false
        });
      })
  };

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  handleInputChange(event) {
    this.setState({
      q: event.target.value
    });
  }

    render() {
      console.log(this.state.data);
      console.log(this.state.value);
      console.log(this.state.q);

      return (
        <div>
          <form action="" onClick={this.handleSubmit}>
            <input type="text" name="q" onChange={this.handleInputChange} />
            <select name="cateogory" value={this.state.value} onChange={this.handleChange}>
              <option value="">Category</option>
              <option value="red">Red</option>
              <option value="yellow">Yellow</option>
              <option value="blue">Blue</option>
              <option value="green">Green</option>
            </select>
            <button>Search</button>
          </form>
          <div className="main-content">
            {this.state.isLoading
              ? <p>Loading</p>
              :
              <ImgList
                data={this.state.data}
              />
            }
          </div>
        </div>
      );
   }
}

export default SearchForm;
