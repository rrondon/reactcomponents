import React from 'react';
import './articlenav.css';

const ArticleNav = ({ description, likes, articleClick, id }) => {
    return (
        <div className="articleNavItem" onClick={() => articleClick(id)}>
             <div className="articleItem">
                <div className="articleDescription">
                   <p className="articleInfo">{description}</p>
                </div>
              </div>
        </div>
    )
}

export default ArticleNav;
