import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import axios from 'axios';
import PreviousArrow from '../PreviousArrow/previousarrow';
import NextArrow from '../NextArrow/nextarrow';

class Carousel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count: 0,
      img: '',
      data: [],
      id: '',
      description: '',
      isLoading: false,
      index: 0
    }
  }

  componentDidMount() {
    this.performSearch();
  }

  performSearch = (query = 'red') => {
    axios
    .get(
      `https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${query}&client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1`
    )
    .then(data => {
      this.setState({
        data: data.data.results,
        isLoading: false
      });
   })
   .catch(err => {
      console.log('Error happeneded during fetching!', err);
      });
  };

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 700,
      prevArrow: <PreviousArrow />,
      nextArrow: <NextArrow />,
      centerMode: false
    };

    return (
      <div className="container">
        <Slider {...settings}>
          <div className="carouselImg">
            <img src="https://images.unsplash.com/photo-1527624374174-e52ac241aab0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"></img>
          </div>
          <div className="carouselImg">
            <img src="https://images.unsplash.com/photo-1506706907212-709cf5fd7f06?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"></img>
          </div>
          <div className="carouselImg">
            <img src="https://images.unsplash.com/photo-1460321089670-2797c614c5ba?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"></img>
          </div>
        </Slider>
      </div>
    );
  }
}

export default Carousel;
