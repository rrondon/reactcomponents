import React from 'react';
import './layout.css';
import Slider from './Slider/slider';

const Layout = () => {
    return (
      <div>
        <Slider />
      </div>
    )
}

export default Layout
