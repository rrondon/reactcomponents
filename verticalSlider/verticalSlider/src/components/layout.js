import React from 'react';
import './layout.css';
import VerticalSlider from './VerticalSlider/VerticalSlider';

const Layout = () => {
    return (
      <div>
        <VerticalSlider />
      </div>
    )
}

export default Layout
