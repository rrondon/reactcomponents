## Vertical Slider ##

## Setup ##

This project was initially setup with Gatsby (https://www.gatsbyjs.org)

## Installation Steps ##

1. `git clone https://rrondon@bitbucket.org/rrondon/reactcomponents.git`
2. `cd reactcomponents/verticalSlider/vertical-slider`
3. Install Font Awesome
   `npm i --save @fortawesome/fontawesome-svg-core`
   `npm i --save @fortawesome/free-solid-svg-icons`
   `npm i --save @fortawesome/react-fontawesome`
4. `npm install`
5. `npm run develop`
6. Visit `localhost:8000` in your browser of choice
