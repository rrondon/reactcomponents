import React, { Component } from 'react';
import './contenthub.css';
import List from '../List/list';

class ContentHub extends Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      img: '',
      data: [],
      id: '',
      description: '',
      isLoading: false,
      likes: 0,
      visible: 8
      }

      this.loadMore = this.loadMore.bind(this);
    }

    loadMore() {
        this.setState((prev) => {
            return {
                visible: prev.visible + 2
            }
        });
    }

  componentDidMount() {
    fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
      method: 'GET',
    })
    .then((results) => {
      results.json().then((data) => {
        this.setState({
          data: data,
          id: data[0].id,
          img: data[0].urls.small,
          description: data[0].description,
          likes: data[0].total_likes,
          isLoading: true
        })
      })
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  }

  componentWillUnmount() {
    this.setState({
      isLoading: false
    })
  }

  showCards() {
    return this.state.data.slice(0, this.state.visible).map((card) => (
      <List
        key={card.id}
        img={card.urls.small}
        description={card.description}
        likes={card.likes}
      />
  ));
  }

  render() {
      return (
        <div className="cardGrid">
          <div className="cardLayout">
            {this.showCards()}
              </div>
              {this.state.visible < this.state.data.length &&
                  <button onClick={this.loadMore} type="button" className="loadMoreBtn">Load More Posts</button>
              }
      </div>
    );
  }
}

export default ContentHub;
