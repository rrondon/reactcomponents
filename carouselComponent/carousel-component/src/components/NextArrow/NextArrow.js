import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';

const NextArrow = (props) => {
  const { onClick } = props;

  return (
    <div
      className="rightArrow arrow"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faAngleRight} />
    </div>
  );
}

export default NextArrow;
