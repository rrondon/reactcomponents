import React from 'react';
import './slide.css';
import Dot from '../Dot/dot';

const Slide = ({ img, id, description, likes, index }) => {
  return (
    <div className="slideItem">
      <div className="slide">
        <img key={index} src={img} alt=""></img>
        <div className="slideDetails">
          <h1 className="slideDescription">{description}</h1>
          <h1 className="slideLikes">Total Likes: {likes}</h1>
        </div>
      </div>
    </div>
  )
}

export default Slide;
