import React, { Component } from 'react';
import axios from 'axios';
import ImgList from '../ImgList/imglist';
import './latest.css';

class Latest extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      data: [],
    }
  }


  componentDidMount() {
    this.renderLatest();
  }

  renderLatest = (query = 'blue') => {
    axios
    .get(
      `https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${query}&client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1`
    )
    .then(data => {
      this.setState({
        data: data.data.results,
        isLoading: false
      });
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  };

  componentWillUnmount() {
    this.setState({
      isLoading: true
    })
  }

  render() {
    return(
      <div className="latestLayout">
        <div className="main-content">
            <ImgList
              data={this.state.data}
            />
        </div>
      </div>
    );
  }
}

export default Latest;
