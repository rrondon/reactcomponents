import React from 'react';
import ContentHub from './ContentHub/contenthub';

const Layout = () => (
    <div>
        <ContentHub />
    </div>
)

export default Layout
