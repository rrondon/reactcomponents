import React, { Component } from 'react';
import './accordionitem.css';

class AccordionItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
      class: 'accordion'
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const { open } = this.state;

    return(
      <div className="accordionContainer">
        <div className={open ? 'openAccordion': 'closeAccordion'}>
          <header className="accordionHeader">
            <div className="accordionIcon"></div>
            <h1 className="accordionItemHeading">
              <a onClick={this.handleClick}>{this.props.description}</a>
            </h1>
          </header>
          <div className="accordionContentDiv">
            <ul className="accordionItemContent">
                <li className="accordionContent">{this.props.description}</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default AccordionItem;
