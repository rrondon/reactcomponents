import React from 'react';
import Search from './Search/search';

const SearchLayout = () => (
    <div>
        <Search />
    </div>
)

export default SearchLayout
