import React, { Component } from 'react';
import './dropdown.css';
import List from '../List/list';
import axios from 'axios';
import ImgList from '../ImgList/imglist';

class Dropdown extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count: 0,
      img: '',
      data: [],
      id: '',
      description: '',
      isLoading: false,
      likes: 0,
      visible: 8,
      label: '',
      value: 'green'
    }
    this.loadMore = this.loadMore.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {
        visible: prev.visible + 2
      }
    });
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.performSearch();
  }

  componentDidMount() {
    this.performSearch();
  }

  performSearch = (query = this.state.value) => {
    axios
    .get(
      `https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${query}&client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1`
    )
    .then(data => {
      this.setState({
        data: data.data.results,
        label: data.data.results[0].description,
        isLoading: false
      });
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
  };

  render() {
    console.log('this is the current value');
    console.log(this.state.value);

    return (
      <div className="dropdownPage">
        <div className="cardGrid">
          <div className="cardLayout">
          <form onSubmit={this.handleSubmit} className="dropdownLayout">
              <select className="dropdownSelect" value={this.state.value} onChange={this.handleChange}>
                <option value="red" onchange={this.handleChange}>Red</option>
                <option value="blue" onchange={this.handleChange}>Blue</option>
                <option value="yellow" onchange={this.handleChange}>Yellow</option>
                <option value="green" onchange={this.handleChange}>Green</option>
              </select>
            <input className="dropdownSubmit" type="submit" value="Search" />
          </form>
            <div className="main-content">
              {this.state.isLoading
                ? <p>Loading</p>
                :
                <ImgList
                  data={this.state.data}
                />
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dropdown;
