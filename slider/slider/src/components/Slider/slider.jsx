import React, { Component } from 'react';
import Slide from '../Slide/slide';
import LeftArrow from '../LeftArrow/leftarrow';
import RightArrow from '../RightArrow/rightarrow';
import Dots from '../Dots/dots';
import './slider.css';

class Slider extends Component {
    constructor(props) {
      super(props)

        this.state = {
          count: 0,
          img: '',
          data: [],
          id: '',
          description: '',
          isLoading: false,
          index: 0
        };
      }

      componentDidMount() {
        fetch('https://api.unsplash.com/photos/?client_id=e3d37a3f6d41c25020197820a612d8b7c0ff0c78f99edba1f82a38ee6eea87f1&query=animal&orientation=portrait', {
          method: 'GET',
        })
        .then((results) => {
          results.json().then((data) => {
            this.setState({
              data: data,
              id: data[0].id,
              img: data[0].urls.full,
              description: data[0].description,
              isLoading: true
            })
          })
        })
        .catch(err => {
          console.log('Error happened during fetching!', err);
        });
      }

      componentWillUnmount = () => {
        this.setState({
          isLoading: false
        })
      }

      goToNextSlide = () => {
        this.setState({
          count: this.state.count + 1,
          img: this.state.data[this.state.count + 1].urls.full,
          id: this.state.data[this.state.count + 1].id,
          description: this.state.data[this.state.count + 1].description,
          index: this.state.index + 1,
          isLoading: true
        })
      }

      goToPrevSlide = () => {
        this.setState({
          count: this.state.count - 1,
          img: this.state.data[this.state.count -1].urls.full,
          id: this.state.data[this.state.count -1].id,
          description: this.state.data[this.state.count -1].description,
          index: this.state.index - 1,
          isLoading: false
          })
    }

    dotClick = (i) => {
      if (i === this.state.index) {
        return
      } else {
        this.setState({
          img: this.state.data[i].urls.full,
          id: this.state.data[i].id,
          index: i
        })
      }
    }


    render() {
        return (
          <div className="horizontalSlider">
            <div className="slider">

                <Slide
                  key={this.state.id}
                  src={this.state.img}
                  description={this.state.description}
                />

              <div className="slideSelectors">
                <LeftArrow
                    goToPrevSlide={this.goToPrevSlide}
                  />

                <Dots
                  index={this.state.index}
                  data={this.state.data}
                  dotClick={this.dotClick}
                />

                <RightArrow
                    goToNextSlide={this.goToNextSlide}
                  />
                </div>

            </div>
          </div>
        );
      }
}

export default Slider;
