import React from 'react';
import './img.css';

const Img = (props) => {
    const { img, id, description, likes} = props

    return (
        <div className="cardDiv">
            <div className="Card">
                <div className="cardContent">
                    <img className="cardImg" key={id} src={img} alt=''></img>
                    <div className="cardDescription">
                        <p className="imgInfo">{description}</p>
                        <p className="imgLikes">Total Likes: {likes}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Img;
